<?php
declare(strict_types=1);

namespace ImaginationMedia\Correios\Model\ResourceModel\Cotacoes;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Bulk\OperationInterface;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Psr\Log\LoggerInterface as Logger;

class Results extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{
    private ResourceConnection $resourceConnection;

    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        $mainTable,
        ResourceConnection $resourceConnection,
        $resourceModel = null,
        $identifierName = null,
        $connectionName = null
    ) {
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $mainTable,
            $resourceModel,
            $identifierName,
            $connectionName
        );
        $this->resourceConnection = $resourceConnection;
        $this->setMainTable($this->resourceConnection->getTableName('correios_cotacoes'));
    }

    /**
     * {@inheritdoc}
     */
    protected function _initSelect()
    {
        $this->getSelect()->from(['main_table' => $this->getMainTable()], '*');
        return $this;
    }
}